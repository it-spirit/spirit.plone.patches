# -*- coding: utf-8 -*-
"""Post install import steps for spirit.plone.patches."""


def post_install(context):
    """Post install script"""
    if context.readDataFile('spiritplonepatches_default.txt') is None:
        return
    # Do something during the installation of this package

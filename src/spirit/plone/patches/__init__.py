# -*- coding: utf-8 -*-
"""Proof of concept patches for Plone."""

from plone import api as ploneapi
from spirit.plone.patches import config

import logging

PLONE_4 = '4' <= ploneapi.env.plone_version() < '5'
PLONE_5 = '5' <= ploneapi.env.plone_version() < '6'

logger = logging.getLogger(config.PROJECT_NAME)

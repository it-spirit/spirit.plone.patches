# -*- coding: utf-8 -*-
"""Configuration options for spirit.plone.patches."""

PROFILE_ID = u'profile-spirit.plone.patches'
INSTALL_PROFILE = '{0}:default'.format(PROFILE_ID)
UNINSTALL_PROFILE = '{0}:uninstall'.format(PROFILE_ID)
PROJECT_NAME = 'spirit.plone.patches'

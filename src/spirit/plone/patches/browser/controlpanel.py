# -*- coding: utf-8 -*-
"""Plone controlpanel patches."""

from plone.app.registry.browser import controlpanel
from Products.CMFPlone.controlpanel.browser import site
from zope.interface import alsoProvides

try:
    from plone.protect.interfaces import IDisableCSRFProtection
except ImportError:
    # Plone 4
    IDisableCSRFProtection = None


class SiteControlPanelForm(site.SiteControlPanelForm):
    """Customized site control panel."""

    def __init__(self, context, request):
        super(SiteControlPanelForm, self).__init__(context, request)
        # Disable CSRF protection because of plone.formwidget.namedfile
        # which is used for the logo field (the IFileUploadTemporaryStorage).
        # This is a temporary storage adapter for file uploads. It is used
        # so one does not need to re-upload files after form submission errors.
        # It causes an annoying confirmation dialog just by opening the
        # site controlpanel once a custom logo is set.
        if IDisableCSRFProtection is not None:
            alsoProvides(request, IDisableCSRFProtection)


class SiteControlPanel(controlpanel.ControlPanelFormWrapper):
    form = SiteControlPanelForm

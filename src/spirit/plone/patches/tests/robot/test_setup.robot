*** Settings ***

Resource  keywords.robot

Test Setup  Open test browser
Test Teardown  Close all browsers


*** Test cases ***

Show how to activate the add-on
    Given a logged-in manager
    and the addons controlpanel

    Page should contain element  xpath=//*[@value='spirit.plone.patches']
    Assign id to element
    ...  xpath=//*[@value='spirit.plone.patches']/ancestor::li
    ...  addons-spirit-plone-patches
    Assign id to element
    ...  xpath=//*[@value='spirit.plone.patches']/ancestor::ul/parent::*/parent::*
    ...  addons-enabled

    Highlight  addons-spirit-plone-patches
    Capture and crop page screenshot
    ...  setup_select_add_on.png
    ...  id=addons-enabled

    Click button  xpath=//*[@value='spirit.plone.patches']/ancestor::form//input[@type='submit']

    Page should contain element  xpath=//*[@value='spirit.plone.patches']

    Assign id to element
    ...  xpath=//*[@value='spirit.plone.patches']/ancestor::li
    ...  addons-spirit-plone-patches
    Assign id to element
    ...  xpath=//*[@value='spirit.plone.patches']/ancestor::ul/parent::*/parent::*
    ...  addons-enabled

    Highlight  addons-spirit-plone-patches
    Capture and crop page screenshot
    ...  setup_select_add_on_installable.png
    ...  id=addons-enabled


*** Keywords ***

the addons control panel
  Go to  ${PLONE_URL}/prefs_install_products_form

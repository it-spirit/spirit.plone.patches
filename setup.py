# -*- coding: utf-8 -*-
"""Setup for spirit.plone.patches package."""

from setuptools import find_packages
from setuptools import setup

version = '0.1.dev0'
description = 'Proof of concept patches for Plone.'
long_description = ('\n'.join([
    open('README.rst').read(),
    'Contributors',
    '------------\n',
    open('CONTRIBUTORS.rst').read(),
    open('CHANGES.rst').read(),
]))


install_requires = [
    'setuptools',
    # -*- Extra requirements: -*-
]

setup(
    name='spirit.plone.patches',
    version=version,
    description=description,
    long_description=long_description,
    # Get more strings from
    # http://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Web Environment',
        'Framework :: Plone',
        'Framework :: Plone :: 5.0',
        'Framework :: Plone :: 5.1',
        'Framework :: Plone :: 5.2',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v2 (GPLv2)',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
    ],
    keywords='plone zope patches',
    author='it-spirit',
    author_email='thomas.massmann@it-spir.it',
    url='https://gitlab.com/it-spirit/spirit.plone.patches',
    download_url='http://pypi.python.org/pypi/spirit.plone.patches',
    license='GPL version 2',
    packages=find_packages('src', exclude=['ez_setup']),
    package_dir={'': 'src'},
    namespace_packages=['spirit', 'spirit.plone'],
    include_package_data=True,
    zip_safe=False,
    extras_require=dict(
        test=[
            'plone.app.contenttypes',
            'plone.app.layout[test]',
            'plone.app.robotframework[debug]',
            'plone.app.testing',
            'plone.app.textfield',
            'plone.namedfile',
            'plone.testing>=5.0.0',
            'robotframework-selenium2screenshots',
        ],
    ),
    install_requires=install_requires,
    entry_points="""
    # -*- Entry points: -*-

    [z3c.autoinclude.plugin]
    target = plone
    """,
)

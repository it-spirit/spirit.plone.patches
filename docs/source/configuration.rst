Installation and Configuration
==============================

Install the Add-On
------------------

You can install the Add-On like any other Plone Add-On: click on your username in the *personal tools menu* and select **Site Setup**.
Next, click the **Add-Ons** item in the *Plone Configuration* section.
You should see an item called **spirit.plone.patches**.

.. image:: ../_images/setup_select_add_on_installable.png

You can remove **spirit.plone.patches** again from your Plone site by clicking the uninstall button next to the add-on:

.. image:: ../_images/setup_select_add_on.png

Changelog
=========

0.1 (unreleased)
----------------

- Initial release.
  [tmassman]

- Add custom Site Controlpanel which disables CSRF protection.
  [tmassman]
